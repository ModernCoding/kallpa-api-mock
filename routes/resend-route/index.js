'use strict'

module.exports = async function (fastify, opts) {
  
  fastify.post ('/', async (request, reply) => {

    try {
      reply.code (200).send (request.body.redirectUrl)
    }


    catch (error) {
      reply.code (200).send ({ error: JSON.stringify (error) })
    }
  
  })

}