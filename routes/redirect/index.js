'use strict'

module.exports = async function (fastify, opts) {
  
  fastify.post ('/', async (request, reply) => {

    try {
      reply.code (200).redirect (302, request.body.redirectUrl)
    }


    catch (error) {
      reply.code (418).send ({ error: JSON.stringify (error) })
    }
  
  })

}